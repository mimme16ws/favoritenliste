# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Mit dieser Anwendung kann ein Nutzer nach Serien und Filmen suchen. Diese kann er in seine persönliche Liste hinzufügen. Serien die derzeit im TV laufen, werden in einen Kalender eingefügt. Somit hat der Nutzer ein Überblick wann seine Serien im Fernsehen ausgestrahlt werden. Außerdem können die Listen anderer Nutzer angesehen werden die ähnliche Interessen haben.

### How do I get set up? ###

Um die Anwendung zu starten, muss nur die Index-Datei ausgeführt werden. Außerdem ist eine Internetverbindung notwendig. Empfohlen wird der Browser Google Chrome, da in den anderen Browsern nicht alle Funktionen unterstützt werden. Es kann passieren, dass die Daten der OMDb-Datenbank nicht immer sofort zur Verfügung stehen. Dies betrifft vor allem die Suchfunktion, die Episodenansicht im Kalender sowie die Seasonansicht in der Detailansicht.

Die Datenbank ist auf der Seite Parse.com angelegt. Die Verbindung zur Datenbank erfolgt durch den Aufruf Parse.initialize der die ApplicationID und JavaScript Key benötigt. Diese sind bereits im Code eingebunden. Außerdem benötigt man einen Zugangscode um auf die OMDb-Datenbank zuzugreifen. Auch hier ist der Zugangscode bereits im Sourcecode vorhanden.

### Contribution guidelines ###



### Who do I talk to? ###
