var FavorizeController = FavorizeController || {};

FavorizeController.CalendarDate = (function () {
    "use strict";
    /* eslint-env browser */
    
    var that = {},
        d = new Date(),
        dm = d.getMonth() + 1,
        dy = d.getYear() + 1900,
        monthName = new Array("January", "February", "March", "April", "May", "June",
                             "July", "August", "September", "October", "November", "December"),
        //Tag = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"),
        time = new Date(dy, dm - 1, 1),
        firstWeekday = time.getDay(),
        daysPerMonth,
        totalFieldsNeeded,
        totalRowsNeeded;
    
    //get the amount of days for the current month    
    function getDaysPerMonth() {
        daysPerMonth = 31;
        if (dy %  4 === 0 && dm === 2) {
            daysPerMonth = 29;
        } else if (dm === 4 || dm === 6 || dm === 9 || dm === 11) {
            daysPerMonth = 30;
        } else if (dm === 2) {
            daysPerMonth = 28;
        }
    }
    
    //get the amount of fields and the total rows needed for the current month
    function getTotalFieldsNeeded() {
        totalFieldsNeeded = daysPerMonth + firstWeekday;
        if (totalFieldsNeeded === 28) {
            totalRowsNeeded = 4;
        } else if (totalFieldsNeeded >= 29 && totalFieldsNeeded <= 35) {
            totalRowsNeeded = 5;
        } else if (totalFieldsNeeded > 35) {
            totalRowsNeeded = 6;
        }
    }
    
    //generate the calendar for the current month
    function generateCalendar() {
        var firstDayOfMonth = new Date(d.getFullYear(), d.getMonth(), 1),
            today = new Date(d.getFullYear(), d.getMonth(), d.getDate()),
            todayDate,
            numberOfToday,
            numberOfCurrentMonth;
        
        getDaysPerMonth();
        getTotalFieldsNeeded();
        numberOfToday = today.getDate();
        if (numberOfToday < 10) {
            numberOfToday = "0" + numberOfToday;
        }
        numberOfCurrentMonth = today.getMonth() + 1;
        if (numberOfCurrentMonth < 10) {
            numberOfCurrentMonth = "0" + numberOfCurrentMonth;
        }
        todayDate = (today.getFullYear()) + "-" + numberOfCurrentMonth + "-" + numberOfToday;
        generateCurrenMonth(firstDayOfMonth, todayDate);
    }
    
    //Generates the calendar for the current month
    function generateCurrenMonth(firstDayOfMonth, todayDate) {
        var dateNumber,
            field,
            currentDayDate,
            currentMonth,
            currentDay,
            firstCounter,
            secondCounter,
            start,
            y,
            z,
            row;
            
        firstCounter = 0;
        secondCounter = 0;
        start = firstWeekday;
        for (y = 0; y < totalRowsNeeded; y++) {
            row = document.createElement("ul");
            row.className = "days";
            document.getElementById("calendar").appendChild(row);
            for (z = 0; z < 7; z++) {
                currentDay = firstDayOfMonth.getDate() + secondCounter;
                if (currentDay < 10) {
                    currentDay = "0" + currentDay;
                }
                currentMonth = firstDayOfMonth.getMonth() + 1;
                if (currentMonth < 10) {
                    currentMonth = "0" + currentMonth;
                }
                currentDayDate = (firstDayOfMonth.getFullYear()) + "-" + currentMonth + "-" + currentDay;
                currentDayDate = (firstDayOfMonth.getFullYear()) + "-" + currentMonth + "-" + currentDay;
                field = document.createElement("li");
                field.className = "day";
                row.appendChild(field);
                dateNumber = document.createElement("div");
                field.appendChild(dateNumber);
                if (firstCounter >= start && secondCounter < daysPerMonth) {
                    field.id = currentDayDate;
                    secondCounter++;
                    dateNumber.innerHTML = secondCounter;
                }
                if (currentDayDate === todayDate) {
                    dateNumber.style.backgroundColor = "#0000FF";
                }
                firstCounter++;
                if (firstCounter > firstWeekday && firstCounter <= totalFieldsNeeded) {
                    dateNumber.className = "date";
                }
            }
        }
        document.getElementById("title").innerHTML = monthName[dm - 1] + " " + dy;
    }
    
    //Loads the Informations of the episodes in the calendar
    function loadEpisodes(data) {
        var i,
            x,
            seasonTitle,
            episodesCount,
            episodeTitle,
            episodeNumber,
            released,
            description,
            currentDay,
            currentMonth,
            currentDate,
            cdID,
            event;
        
        for (x = 0; x < data.length; x++){
            if (data[x].Response !== "False") {
                seasonTitle = data[x].Title;
                episodesCount = data[x].Episodes.length;
                for (i = 0; i < episodesCount; i++) {
                    episodeTitle = data[x].Episodes[i].Title;
                    episodeNumber = data[x].Episodes[i].Episode;
                    released = data[x].Episodes[i].Released;

                    for (currentDay = 1; currentDay <= daysPerMonth;  currentDay++) {
                        if (currentDay < 10) {
                            currentDay = "0" + currentDay;
                        }
                        currentMonth = d.getMonth() + 1;
                        if (currentMonth < 10) {
                            currentMonth = "0" + currentMonth;
                        }
                        currentDate = d.getFullYear() + "-" + currentMonth + "-" + currentDay;
                        if (currentDate === released) {
                            cdID = document.getElementById(currentDate);
                            event = document.createElement("div");
                            event.className = "event";
                            event.id = x;
                            description = document.createElement("div");
                            description.className = "event-description";
                            description.innerHTML = released + "<br />" + seasonTitle + ": " + episodeTitle;
                            cdID.appendChild(event);
                            event.appendChild(description);
                        }
                    }
                }
            }
        }
    }

    that.loadEpisodes = loadEpisodes;
    that.generateCalendar = generateCalendar;
    return that;
}());