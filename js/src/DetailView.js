var FavorizeController = FavorizeController || {};

FavorizeController.DetailView = (function () {
    "use strict";
    /* eslint-env browser  */
    
    var that = {},
        storage,
        login,
        headerFunctions,
        view,
        server,

        seriesItem,
        listOfLoggedInUser = [],

        detailAddButton = document.getElementById("detailAddButton"),
        detailDeleteButton = document.getElementById("detailDeleteButton"),
        informationForTheUser = document.getElementById("information-for-user"),
        wordSignUpDetail = document.getElementById("detail-signup"),
        wordLoginDetail = document.getElementById("detail-login"),

        userStatus;


    
    //Loads the list of the current user
    function getSeries() {
        storage.loadSeriesOrMovieItem();
        seriesItem = storage.getSeriesOrMovieItem();
    }


    //Saves a series/ movie when the "add" button was clicked
    function saveSeriesToList() {
        if (seriesItem.Poster === "N/A") {
            seriesItem.Poster = "res/images/imageNotAvailable.png";
        }
        login.saveSeriesForUser(seriesItem);
    }


    //Removes series/ movie when the "delete" button was clicked
    function deleteSeriesFromList() {
        login.removeSeriesFromUserList(seriesItem);
    }
    
    
    //Shows the login window when the user clicks the word "log-in" in the detail view
    function showLoginWindow() {
        view.showOrHideLoginDialog();
    }
    
    
    //Shows the signup window when the user clicks the word "sign up" in the detail view
    function showRegisterWindow() {
        view.showOrHideRegisterDialog();
    }


    //Setups the listener on the buttons
    function setListenerOnButtons() {
        detailAddButton.addEventListener("click", saveSeriesToList);
        detailDeleteButton.addEventListener("click", deleteSeriesFromList);
        wordLoginDetail.addEventListener("click", showLoginWindow);
        wordSignUpDetail.addEventListener("click", showRegisterWindow);
    }


    //Depending on the situation shows or hides buttons
    function showButtonsInView() {
        var serPresent,
            usersListCount;
        

        listOfLoggedInUser = storage.getList();

        if (userStatus !== "false") {
            if (listOfLoggedInUser !== null) {
                for (usersListCount = 0; usersListCount < listOfLoggedInUser.length; usersListCount++) {
                    if (listOfLoggedInUser[usersListCount].imdbID === seriesItem.imdbID) {
                        serPresent = true;
                    }
                }
            }
            
            if (serPresent) {
                detailDeleteButton.style.display = "block";
                detailAddButton.style.display = "none";
                informationForTheUser.style.display = "none";
            } else {
                detailAddButton.style.display = "block";
                detailDeleteButton.style.display = "none";
                informationForTheUser.style.display = "none";
            }
        } else {
            detailAddButton.style.display = "none";
            detailDeleteButton.style.display = "none";
            informationForTheUser.style.display = "block";
        }
    }
    
    
    //Loads the seasons information from the ajax call into the seasonDetails
    function loadSeasonDetails(event) {
        var seasonsViewID,
            allSeasons,
            totalSeasons,
            seasonOverview;
        //Shows Season Details for Series
        if (event.data[0].Response === "True") {
            document.getElementById("seasonsView").style.display = "block";
        }
        allSeasons = event.data;
        totalSeasons = event.data.length;
        seasonsViewID = document.getElementById("seasonsView");
        seasonOverview = document.createElement("div");
        seasonOverview.className = "seasonsOverview";
        seasonsViewID.appendChild(seasonOverview);
        generateSeasonOverview(allSeasons, totalSeasons, seasonOverview);
    }
    
    
    //Generates the overview of the seasons
    function generateSeasonOverview(allSeasons, totalSeasons, seasonOverview) {
        var i, seasonCounter,
            seasonNumber,
            totalEpisodes,
            currentSeason,
            seasonHeader,
            seasonButton,
            episodeHeader,
            titleHeader,
            releasedHeader,
            seasonDetails;
        for (i = 0; i < totalSeasons; i++) {
            currentSeason = allSeasons[i].Episodes;
            totalEpisodes = currentSeason.length;
            seasonNumber = i + 1;
            seasonCounter = document.createElement("div");
            seasonButton = document.createElement("button");
            seasonButton.innerHTML = "Season:" + seasonNumber;
            seasonButton.className = "season-button";
            seasonButton.id = "season-" + seasonNumber;
            seasonCounter.appendChild(seasonButton);
            seasonOverview.appendChild(seasonCounter);
            seasonHeader = document.createElement("ul");
            seasonHeader.className = "seasonParameters";
            seasonHeader.id = "season-" + seasonNumber + "-parameters";
            seasonHeader.style.display = "none";
            seasonCounter.appendChild(seasonHeader);
            episodeHeader = document.createElement("li");
            episodeHeader.innerHTML = "Episode";
            seasonHeader.appendChild(episodeHeader);
            titleHeader = document.createElement("li");
            titleHeader.innerHTML = "Titel";
            seasonHeader.appendChild(titleHeader);
            releasedHeader = document.createElement("li");
            releasedHeader.innerHTML = "Released";
            seasonHeader.appendChild(releasedHeader);
            seasonDetails = document.createElement("ul");
            seasonDetails.className = "seasonDetails";
            seasonDetails.style.display = "none";
            seasonCounter.appendChild(seasonDetails);
            seasonDetails.id = "season-" + seasonNumber + "-details";
            seasonButton.addEventListener("click", function () {
                dropDownOrUp(this.id);
            });
            generateEpisodesDetails(totalEpisodes, seasonDetails, currentSeason);
            
        }
    }
    
    
    //Generates the details of the episodes
    function generateEpisodesDetails(totalEpisodes, seasonDetails, currentSeason) {
        var liCurrentEpisode, x,
            currentEpisode,
            released,
            title,
            liReleased,
            liTitle;
        for (x = 0; x < totalEpisodes; x++) {
            currentEpisode = currentSeason[x].Episode;
            released = currentSeason[x].Released;
            title = currentSeason[x].Title;
            liCurrentEpisode = document.createElement("li");
            liReleased = document.createElement("li");
            liTitle = document.createElement("li");
            liCurrentEpisode.innerHTML = currentEpisode;
            liTitle.innerHTML = title;
            liReleased.innerHTML = released;
            seasonDetails.appendChild(liCurrentEpisode);
            seasonDetails.appendChild(liTitle);
            seasonDetails.appendChild(liReleased);
        }
    }
    
    
    //Shows or hides the season details on screen                              
    function dropDownOrUp(id) {
        var display,
            seasonHeader;
        
        seasonHeader = document.getElementById(id + "-parameters");
        display = document.getElementById(id + "-details");
        if (display.style.display === "none") {
            seasonHeader.style.display = "block";
            display.style.display = "block";
        } else {
            seasonHeader.style.display = "none";
            display.style.display = "none";
        }
        
    }
    

    //Shows or hides buttons on the screen
    function showDataDependingOnLoginStatus(event) {
        userStatus = event.data;
        view.changeViewForLoginAndRegister(event.data);
        login.createUsersListOfSeries("detailOfSeriesOrMovie");
    }


    //Shows/ hides buttons on the screen
    function setDisplayOfButtons() {
        showButtonsInView();
    }


    //Shows the list of the current user
    function showPersonalListOfCurrentUser() {
        location.assign("liste.html");
    }


    //Login of a user and hides the login window and the overlay
    function executeLogin(event) {
        login.loginAsUser(event.data);
        view.showOrHideLoginDialog();
    }


    //Register of a user and hides the register window and the overlay
    function executeSignUp(event) {
        login.registerNewUser(event.data);
        view.showOrHideRegisterDialog();
    }

    
    //Logs out the current user
    function logoutTheCurrentUser() {
        login.logoutAsUser();
    }


    //Loads the list of a current user
    function getListOfCurrentUser() {
        login.createUsersListOfSeries("list");
    }
  

    //Initializes the Event Listener
    function initializeEventListener() {
        login.addEventListener("loginStatusOfUser", showDataDependingOnLoginStatus);
        login.addEventListener("listLoaded", setDisplayOfButtons);
        login.addEventListener("seriesOfAUserLoaded", showPersonalListOfCurrentUser);
        server.addEventListener("SeasonDetailsLoaded", loadSeasonDetails);

        headerFunctions.addEventListener("loginButtonInDialogClicked", executeLogin);
        headerFunctions.addEventListener("registerButtonInDialogClicked", executeSignUp);
        headerFunctions.addEventListener("logoutCurrentUser", logoutTheCurrentUser);
        headerFunctions.addEventListener("listClicked", getListOfCurrentUser);
    }


    //Gets the clicked series or movie, checks the current user and shows the series/ movie on the screen
    function init() {
        storage = FavorizeController.SessionStorage;
        login = FavorizeController.LoginScreen();
        view = FavorizeController.SeriesView();
        headerFunctions = FavorizeController.FunctionsInHeader();
        server = FavorizeController.ServerConnector();
        initializeEventListener();

        getSeries();

        login.init();
        login.getUserStatus();
        view.showSeriesData(seriesItem);
        server.searchSeasonDetails(seriesItem);

        setListenerOnButtons();

    }

    that.init = init;
    return that;
}());