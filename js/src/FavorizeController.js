var FavorizeController = FavorizeController || {};
FavorizeController = (function () {
    "use strict";
    /* eslint-env browser  */
    var that = {},
        login,
        server,
        view,
        headerFunctions,
        calendar,
        storage;


    //Loads the Data into the calender
    function showDetailsOfEpisodesInCalender(event) {
        calendar.loadEpisodes(event.data);
    }
    
    
    //Gets the necessary data of series to load them into the calender
    function loadDataInCalender(event) {
        server.loadSeries(event.data);
    }
    
    
    //Shows login, logout, register or list buttons depending on the status of the user
    function showDataDependingOnLoginStatus(event) {
        if (event.data !== "false") {
            login.createUsersListOfSeries("calender");
        } else {
            login.getTop20();
        }
        view.changeViewForLoginAndRegister(event.data);
    }

    
    //Loads the list screen if the user clicked the list button
    function showPersonalListOfCurrentUser() {
        location.assign("liste.html");
    }
    
    
    //Stores the input of the user and loads the search screen
    function searchForSeriesAndFilms(event) {
        storage.setUserInput(event.data, "save");
        location.assign("search.html");
    }

    
    //Login a user with his data (username and passwort) and hides the login window
    function executeLogin(event) {
        login.loginAsUser(event.data);
        view.showOrHideLoginDialog();
    }

    
    //Register a new user with his data (username, password and email adress) and automatically logs him in
    function executeSignUp(event) {
        login.registerNewUser(event.data);
        view.showOrHideRegisterDialog();
    }


    //Logout the current user and changes the displayed buttons in the header
    function logoutTheCurrentUser() {
        login.logoutAsUser();
    }


    //Loads the list of series and films of the current logged in user
    function getListOfCurrentUser() {
        login.createUsersListOfSeries("list");
    }


    //Setups the Event Listener for differen javascript data
    function setupEventListeners() {
        server.addEventListener("showEpisodesDetails", showDetailsOfEpisodesInCalender);

        login.addEventListener("listFilled", loadDataInCalender);
        login.addEventListener("listForCalenderLoaded", loadDataInCalender);
        login.addEventListener("loginStatusOfUser", showDataDependingOnLoginStatus);
        login.addEventListener("seriesOfAUserLoaded", showPersonalListOfCurrentUser);

        view.addEventListener("userSearchInput", searchForSeriesAndFilms);

        headerFunctions.addEventListener("loginButtonInDialogClicked", executeLogin);
        headerFunctions.addEventListener("registerButtonInDialogClicked", executeSignUp);
        headerFunctions.addEventListener("logoutCurrentUser", logoutTheCurrentUser);
        headerFunctions.addEventListener("listClicked", getListOfCurrentUser);
    }


    //Initializes the connection to the parse.com server, checks the status of the user and loads the list of series for the calender
    function initializeLoginAndParse() {
        login.init();
        login.getUserStatus();
    }
         
    
    //Initializes the javascript data and some functions in the SeriesView
    function init() {
        login = FavorizeController.LoginScreen();
        server = FavorizeController.ServerConnector();
        calendar = FavorizeController.CalendarDate;
        view = FavorizeController.SeriesView();
        headerFunctions = FavorizeController.FunctionsInHeader();
        storage = FavorizeController.SessionStorage;

        setupEventListeners();
        initializeLoginAndParse();
        calendar.generateCalendar();
        view.initializeSearchField();
    }

    that.init = init;
    return that;
}());