var FavorizeController = FavorizeController || {};

FavorizeController.FunctionsInHeader = function () {
    "use strict";
    /* eslint-env browser  */
    /* global EventPublisher */
    var that = new EventPublisher(),
        delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })(),

        view = FavorizeController.SeriesView(),
        storage = FavorizeController.SessionStorage,

        loginButton = document.getElementById("login-Button"),
        registerButton = document.getElementById("register-Button"),
        listButton = document.getElementById("list-Button"),
        logoutButton = document.getElementById("logout-Button"),

        loginNameField = document.getElementById("user-login"),
        loginPasswordField = document.getElementById("password-login"),

        nameFieldRegister = document.getElementById("user-register"),
        passwordFieldRegister = document.getElementById("password-register"),
        emailFieldRegister = document.getElementById("email-register"),

        registerButtonInDialog = document.getElementById("button-register"),
        loginButtonInDialog = document.getElementById("button-login"),

        blurOutBackground = document.getElementById("overlay"),

        logoImage = document.getElementById("header-image"),

        searchFieldHeader = document.getElementById("search-header"),
        searchButtonHeader = document.getElementById("execute-search"),

        currentSite = location.pathname.split("/").slice(-1);


    //Shows the login window on the screen
    function showLoginDialog() {
        view.showOrHideLoginDialog();
    }


    //Shows the registration window on the screen
    function showRegisternDialog() {
        view.showOrHideRegisterDialog();
    }


    //List buttons was clicked
    function showListOfCurrentUser() {
        that.notifyAll("listClicked", null);
    }


    //Logout the current user and if the user is in his list view he will be automatically redirected to the home screen 
    function logoutCurrentUser() {
        $(".event").remove();
        that.notifyAll("logoutCurrentUser", null);
        if (currentSite[0] === "liste.html") {
            setTimeout(function () {
                location.replace("index.html");
            }, 100);
        }
    }


    //Login the current user with his name and password
    function loginAsUser() {
        var nameAndPasswordOfUser = {
            user: loginNameField.value,
            password: loginPasswordField.value
        };
        loginNameField.value = "";
        loginPasswordField.value = "";
        that.notifyAll("loginButtonInDialogClicked", nameAndPasswordOfUser);
    }


    //Sign up a new user with the username, password and the email adress
    function registerNewUser() {
        var newUserData = {
            user: nameFieldRegister.value,
            password: passwordFieldRegister.value,
            email: emailFieldRegister.value
        };
        nameFieldRegister.value = "";
        passwordFieldRegister.value = "";
        emailFieldRegister.value = "";
        that.notifyAll("registerButtonInDialogClicked", newUserData);
    }


    //Closes the login or register window and hides the overlay
    function closeLoginOrRegisterDialog() {
        view.hideRegisterOrLoginDialog();
    }


    //Hover over the logo changes the image
    function changeLogoToGloving() {
        logoImage.src = "res/images/logoHover.png";
    }


    //Changes the logo to normal when the pointer is not on the logo
    function changeLogoToNormal() {
        logoImage.src = "res/images/logoStandart.png";
    }


    //Click on the logo redirects the user to the home screen if the user is not already there
    function showStartScreen() {
        if (currentSite[0] !== "index.html") {
            location.assign("index.html");
        }
    }


    //Gets the input of the user from the home screen or from the header search field
    function searchForSeries() {
        var input = searchFieldHeader.value;
        
        if (currentSite[0] !== "search.html") {
            storage.setUserInput(input, "save");
            location.assign("search.html");
        } else {
            delay(function () {
                that.notifyAll("inputOfUserInHeaderSearchField", input);
            }, 400);
        }
    }


    //Setups click listener and hover on the logo
    function functionsInHeader() {
        loginButton.onclick = function () { showLoginDialog(); };
        registerButton.onclick = function () { showRegisternDialog(); };
        listButton.onclick = function () { showListOfCurrentUser(); };
        logoutButton.onclick = function () { logoutCurrentUser(); };

        loginButtonInDialog.onclick = function () { loginAsUser(); };
        registerButtonInDialog.onclick = function () { registerNewUser(); };

        blurOutBackground.onclick = function () { closeLoginOrRegisterDialog(); };

        logoImage.onmouseover = function () { changeLogoToGloving(); };
        logoImage.onmouseout = function () { changeLogoToNormal(); };
        logoImage.onclick = function () { showStartScreen(); };
        searchFieldHeader.onkeyup = function () { searchForSeries(); };
        searchButtonHeader.onclick = function () { searchForSeries(); };
        
        emailFieldRegister.onkeyup = function (key) {
            if (key.keyCode === 13) {
                registerNewUser();
            }
        };
        
        loginPasswordField.onkeyup = function (key) {
            if (key.keyCode === 13) {
                loginAsUser();
            }
        };
    }

    functionsInHeader();
    return that;
};