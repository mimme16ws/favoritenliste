/* eslint-env browser */
Header = (function () {
    "use strict";
    var that = {},
        header = document.querySelector(".header");


    //Initialize header (logo, buttons, login- and register screen)
    function init() {

        //HTML code for logo searchfield and buttons in the header
        var headerNode = Node.fromString(
            "<header class='row'>" +
                "<div class='c3'>" +
                    "<img id='header-image' src='res/images/logoStandart.png'>" +
                "</div>" +
                "<div class='c6' id='search-field-header'>" +
                    "<div id='searchInHeader' style='display: none'><br>" +
                        "<input id='search-header' type='text' placeholder='Search for Series'/>" +
                        "<button type='button' id='execute-search'></button>" +
                    "</div>" +
                "</div>" +
                "<div class='c3'>" +
                    "<button class='buttonTop' id='login-Button' type='button' style='display: none'>Login</button>" +
                    "<button class='buttonTop' id='logout-Button' type='button' style='display: none'>Logout</button>" +
                    "<button class='buttonTop' id='register-Button' type='button' style='display: none'>Register</button>" +
                    "<button class='buttonTop' id='list-Button' type='button' style='display: none'>List</button>" +
                "</div>" +
                "</header>"
        ),

            //HTML code for the register window
            registerNode = Node.fromString(
                "<div class='md-modal md-effect-16' id='register'>" +
                    "<div class='md-content'>" +
                        "<h3>Sign Up</h3>" +
                        "<p class='input-info'>Enter a username</p>" +
                        "<input type='text' id='user-register' class='input-fields-header' placeholder='Username'>" +
                        "<p class='input-info'>Enter a password</p>" +
                        "<input type='password' id='password-register' class='input-fields-header' placeholder='Passwort'>" +
                        "<p class='input-info'>Enter your e-mail address</p>" +
                        "<input type='text' id='email-register' class='input-fields-header' placeholder='E-mail'>" +
                        "<button id='button-register' class='buttons-window' type='button'>Register</button>" +
                    "</div>" +
                    "</div>"
            ),

            //HTML code for the login window
            loginNode = Node.fromString(
                "<div class='md-modal md-effect-16' id='login'>" +
                    "<div class='md-content'>" +
                        "<h3>Login</h3>" +
                        "<p class='input-info'>Enter your username</p>" +
                        "<input type='text' id='user-login' class='input-fields-header' placeholder='Username'>" +
                        "<p class='input-info'>Enter your password</p>" +
                        "<input type='password' id='password-login' class='input-fields-header' placeholder='Passwort'>" +
                        "<button id='button-login' class='buttons-window' type='button'>Login</button>" +
                    "</div>" +
                    "</div>"
            ),

            //Overlay that blur out the background if login or register window is shown
            overlayNode = Node.fromString(
                "<div class='md-overlay' id='overlay'></div>"
            );


        //Appends the above initialized Nodes in the HTML document
        header.appendChild(headerNode);
        header.appendChild(registerNode);
        header.appendChild(loginNode);
        header.appendChild(overlayNode);
    }
    
    that.init = init;
    return that;
}());