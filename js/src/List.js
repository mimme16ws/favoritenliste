var FavorizeController = FavorizeController || {};

FavorizeController.List = (function () {
    "use strict";
    /* eslint-env browser  */
    
    var that = {},
        storage,
        login,
        headerFunctions,
        view,
        seriesListOfAUser,
        allListsOfAllUsers = [],
        userAndNumberOfSameSeriesOrMovies = [];


    
    //Gets the list of a user that is stored in the session storage
    function getSeriesList() {
        storage.loadSeriesList();
        seriesListOfAUser = storage.getList();
    }


    //Saves the clicked series and shows detail of the series/ movie on the screen
    function showSeries(event) {
        storage.setSeriesOrMovieItem(event.data, "save");
        location.assign("itemDetailView.html");
    }


    //Delets a series/ movie from the list of the user
    function removeSeriesFromParse(event) {
        login.removeSeriesFromUserList(event.data);
    }
    
    
    //
    function showListOfClickedUser(event) {
        login.showListOfClickedUserInView(event.data);
    }


    //Shows different buttons, depending on the user status
    function showDataDependingOnLoginStatus(event) {
        if (event.data === "false") {
            location.replace("index.html");
        } else {
            view.changeViewForLoginAndRegister(event.data);
        }
    }
    
    
    // Gets all series/ movies of all users which are saved in parse.com
    function getAllSeriesOrMoviesOfOtherUsers(event) {
        var allUsersInDatabase = event.data,
            i;
        
        for (i = 0; i < allUsersInDatabase.length; i++) {
            login.getSeriesOrMoviesOfSpecificUser(allUsersInDatabase[i], (i + 1), allUsersInDatabase.length);
        }
    }
    
    //
    function loadNewListOfSeriesAndMovies() {
        getSeriesList();
    }
    
    
    //Sorts the users by the common number of series/ movies in the list of the current user and the users in Parse.com database
    function sortListOfSeriesAndMoviesOfOtherUsers() {
        userAndNumberOfSameSeriesOrMovies.sort(function (a, b) {
            return b.commonTotalResults - a.commonTotalResults;
        });
    }
    
    
    //Creates a list of all Users and counts all items in their list by the list items type 
    function createListOfUsersAndTheirList(event) {
        var countOfUser,
            movieCount,
            seriesCount,
            countObjectInOtherList,
            countOwnSeriesOrMovies,
            seriesOrMovieInOtherUsersList,
            seriesOrMovieInOwnList,
            userAndCommonSeriesMoviesCount;
        
        allListsOfAllUsers = event.data;
    
        for (countOfUser = 0; countOfUser < allListsOfAllUsers.length; countOfUser++) {
            movieCount = 0;
            seriesCount = 0;
            for (countObjectInOtherList = 0; countObjectInOtherList < allListsOfAllUsers[countOfUser].seriesAndMovies.length; countObjectInOtherList++) {
                for (countOwnSeriesOrMovies = 0; countOwnSeriesOrMovies < seriesListOfAUser.length; countOwnSeriesOrMovies++) {
                    seriesOrMovieInOtherUsersList = allListsOfAllUsers[countOfUser].seriesAndMovies[countObjectInOtherList];
                    seriesOrMovieInOwnList = seriesListOfAUser[countOwnSeriesOrMovies];
                    
                    if (seriesOrMovieInOtherUsersList.imdbID === seriesOrMovieInOwnList.imdbID) {
                        
                        if (seriesOrMovieInOwnList.Type === "series") {
                            seriesCount++;
                        } else {
                            movieCount++;
                        }
                    }
                }
            }
            
            userAndCommonSeriesMoviesCount = {
                user: allListsOfAllUsers[countOfUser].user,
                userName: allListsOfAllUsers[countOfUser].userName,
                commonSeries: seriesCount,
                commonMovies: movieCount,
                commonTotalResults: seriesCount + movieCount,
                totalNumberOfSeriesAndMovies: allListsOfAllUsers[countOfUser].seriesAndMovies.length
            };
            userAndNumberOfSameSeriesOrMovies.push(userAndCommonSeriesMoviesCount);
        }
        
        sortListOfSeriesAndMoviesOfOtherUsers();
        view.showOtherUsersSeriesAndMoviesInList(userAndNumberOfSameSeriesOrMovies);
    }
    
    
    //Shows the List of a clicked user
    function showListOfOtherUser() {
        location.assign("usersList.html");
    }


    //Logs out the current user 
    function logoutTheCurrentUser() {
        login.logoutAsUser();
    }


    //Initializes different Event Listener
    function initializeEventListener() {
        login.addEventListener("loginStatusOfUser", showDataDependingOnLoginStatus);
        login.addEventListener("listOfUsers", getAllSeriesOrMoviesOfOtherUsers);
        login.addEventListener("listLoaded", loadNewListOfSeriesAndMovies);
        login.addEventListener("dataOfUsersIsAvailable", createListOfUsersAndTheirList);
        login.addEventListener("listOfOtherUserIsLoaded", showListOfOtherUser);

        headerFunctions.addEventListener("logoutCurrentUser", logoutTheCurrentUser);
        
        view.addEventListener("userClickedASeriesInSearchScreenOrList", showSeries);
        view.addEventListener("removeSeriesOrMovieFromPersonalList", removeSeriesFromParse);
        view.addEventListener("clickedUserInListViewScreen", showListOfClickedUser);
    }


    //Shows series when the top series button is clicked
    function showSeriesInList() {
        view.showSeriesInListScreen();
    }


    //Shows movies when the top movie button is clicked
    function showMoviesInList() {
        view.showMoviesInListScreen();
    }


    //Sets the click listener on the top series and movies buttons
    function setupButtonsInList() {
        document.getElementById("series-show").addEventListener("click", showSeriesInList);
        document.getElementById("movies-show").addEventListener("click", showMoviesInList);
    }
    
    

    //Initializes the list with series/ movies of the current logged in user
    function init() {
        storage = FavorizeController.SessionStorage;
        login = FavorizeController.LoginScreen();
        view = FavorizeController.SeriesView();
        headerFunctions = FavorizeController.FunctionsInHeader();
        initializeEventListener();

        login.init();
        login.getUserStatus();

        getSeriesList();

        view.showSeriesOrFilmsInSearchScreenOrListView(seriesListOfAUser);
        
        setupButtonsInList();
        
        login.createlistOfOtherUsers();
    }

    that.init = init;
    return that;
}());