var FavorizeController = FavorizeController || {};

FavorizeController.ListOfOtherUser = (function () {
    "use strict";
    /* eslint-env browser  */
    
    var that = {},
        storage,
        login,
        headerFunctions,
        view,
        seriesListOfAUser;


    
    //Gets the list of a user that is stored in the session storage
    function getSeriesListOfClickedUser() {
        storage.loadListOfClickedUser();
        seriesListOfAUser = storage.getListOfClickedUser();
    }


    //Saves the clicked series and shows detail of the series/ movie on the screen
    function showSeries(event) {
        storage.setSeriesOrMovieItem(event.data, "save");
        location.assign("itemDetailView.html");
    }


    //Shows different buttons, depending on the user status
    function showDataDependingOnLoginStatus(event) {
        if (event.data === "false") {
            location.replace("index.html");
        } else {
            view.changeViewForLoginAndRegister(event.data);
        }
    }


    //Logs out the current user 
    function logoutTheCurrentUser() {
        login.logoutAsUser();
    }


    //Initializes different Event Listener
    function initializeEventListener() {
        login.addEventListener("loginStatusOfUser", showDataDependingOnLoginStatus);

        headerFunctions.addEventListener("logoutCurrentUser", logoutTheCurrentUser);
        
        view.addEventListener("userClickedASeriesInSearchScreenOrList", showSeries);
    }


    //Shows series when the top series button is clicked
    function showSeriesInList() {
        view.showSeriesInListScreen();
    }


    //Shows movies when the top movie button is clicked
    function showMoviesInList() {
        view.showMoviesInListScreen();
    }


    //Sets the click listener on the top series and movies buttons
    function setupButtonsInList() {
        document.getElementById("series-show").addEventListener("click", showSeriesInList);
        document.getElementById("movies-show").addEventListener("click", showMoviesInList);
    }
    
    

    //Initializes the list with series/ movies of the current logged in user
    function init() {
        storage = FavorizeController.SessionStorage;
        login = FavorizeController.LoginScreen();
        view = FavorizeController.SeriesView();
        headerFunctions = FavorizeController.FunctionsInHeader();
        initializeEventListener();

        login.init();
        login.getUserStatus();

        getSeriesListOfClickedUser();

        view.showSeriesOrFilmsInSearchScreenOrListView(seriesListOfAUser);
        
        setupButtonsInList();
        
    }

    that.init = init;
    return that;
}());