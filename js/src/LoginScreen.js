var FavorizeController = FavorizeController || {};

FavorizeController.LoginScreen = function () {
    "use strict";
    /* eslint-env browser  */
    /* global EventPublisher */
    var that = new EventPublisher(),
        top20Series = [],
        storage,
        allListsOfAllUsers = [],

        noUser = "false";

        

    //Gets all series of the currently user
    function createUsersListOfSeries(listButtonClicked) {
        var allSeries = [],
            series = Parse.Object.extend("SeriesOfUsers"),
            query = new Parse.Query(series),
            count;
        query.equalTo("user", Parse.User.current());
        query.find().then(
            function (results) {
                for (count = 0; count < results.length; count++) {
                    allSeries.push(results[count].get("seriesItem"));
                }
                storage.setList(allSeries, "save");
            }
        ).then(function () {
            if (listButtonClicked === "list") {
                that.notifyAll("seriesOfAUserLoaded", null);
            } else if (listButtonClicked === "calender") {
                that.notifyAll("listForCalenderLoaded", allSeries);
            } else if (listButtonClicked === "add" || listButtonClicked === "delete" || listButtonClicked === "detailOfSeriesOrMovie") {
                that.notifyAll("listLoaded", null);
            }
        },
            function (error) {
                alert("Error: " + error.code + " " + error.message);
            });
    }
  
   
    //Deletes a series/ movie from the parse.com database of the current user 
    function removeSeriesFromUserList(itemToDelete) {
        var item = Parse.Object.extend("SeriesOfUsers"),
            query = new Parse.Query(item);

        query.equalTo("user", Parse.User.current());
        query.equalTo("seriesImdbId", itemToDelete.imdbID);
        query.find().then(function (results) {
            return query.get(results[0].id);
        }).then(function (seriesDelete) {
            seriesDelete.destroy({});
        }).then(function () {
            setTimeout(function () {
                if (itemToDelete.Type === "series") {
                    alert("The series '" + itemToDelete.Title + "' was removed from your list.");
                } else {
                    alert("The movie '" + itemToDelete.Title + "' was removed from your list.");
                }
            }, 500);
            
            createUsersListOfSeries("delete");
        },
            function (error) {
                alert("Error: " + error.code + " " + error.message);
            });
    }


    //Saves a new Object with its name, imdb ID, the objekt itself and the userID in parse.com
    function saveSeriesForUser(seriesToAdd) {
        var UserSeries = Parse.Object.extend("SeriesOfUsers"),
            series = new UserSeries();
        
        series.set("seriesName", seriesToAdd.Title);
        series.set("seriesImdbId", seriesToAdd.imdbID);
        series.set("user", Parse.User.current());
        series.set("seriesItem", seriesToAdd);
        
        series.save().then(function () {
            if (seriesToAdd.Type === "series") {
                alert("The series '" + seriesToAdd.Title + "' was added to your list.");
            } else {
                alert("The movie '" + seriesToAdd.Title + "' was added to your list.");
            }
            createUsersListOfSeries("add");
        },
                           
            function (error) {
                alert("Error: " + error.code + " " + error.message);
            });
    }
    

    //Checks if a user is logged in
    function checkLoginStatus() {
        var currentUser = Parse.User.current();
        $(".event").remove();
        if (currentUser) {
            that.notifyAll("loginStatusOfUser", currentUser);
        } else {
            that.notifyAll("loginStatusOfUser", noUser);
        }
    }


    //Saves data of the new registrated User on the website parse.com 
    function registerNewUser(newUserData) {
        var user = new Parse.User();
        user.set("username", newUserData.user);
        user.set("password", newUserData.password);
        user.set("email", newUserData.email);

        user.signUp(null, {
            success: function () {
                checkLoginStatus();
                alert("Sign up as '" + newUserData.user + "' was successful.");
            },
            error: function (user, error) {
                alert("Error: " + error.message);
            }
        });
    }


    //Logout of the current user from parse.com
    function logoutAsUser() {
        var nameOfCurrentUser = Parse.User.current().getUsername();
        Parse.User.logOut();
        storage.clearList();
        checkLoginStatus();
        alert("Logout as '" + nameOfCurrentUser + "' was successful.");
    }

    
    
    //Login as existing user to get series in the list and save new series of the user
    function loginAsUser(userDataLogin) {
        Parse.User.logIn(userDataLogin.user, userDataLogin.password, {
            success: function () {
                checkLoginStatus();
                alert("Hello '" + userDataLogin.user + "' :)");
            },
            error: function () {
                alert("Error: Please check your username or password and try it again.");
            }
        });
    }
    
    
    //Saves the list of series/ movies of the clicked user in the list view
    function showListOfClickedUserInView(userName) {
        var allSeries = [],
            series = Parse.Object.extend("SeriesOfUsers"),
            query = new Parse.Query(series),
            count;
        
        query.equalTo("user", userName);
        
        query.find().then(
            function (results) {
                for (count = 0; count < results.length; count++) {
                    allSeries.push(results[count].get("seriesItem"));
                }
                storage.setListOfClickedUser(allSeries, "save");
            }
        ).then(function () {
            that.notifyAll("listOfOtherUserIsLoaded", null);
        },
            function (error) {
                alert("Error: " + error.code + " " + error.message);
            });
    }
    
    
    //Gets all series/ movies of a specific user from the database parse.com 
    function getSeriesOrMoviesOfSpecificUser(userName, currentUserCount, totalNumberOfUser) {
        var seriesAndMoviesCount,
            seriesAndMovies = [],
            userNumberOfItemsAndItems,
            seriesMoviesAll = Parse.Object.extend("SeriesOfUsers"),
            query = new Parse.Query(seriesMoviesAll);
        
        
        query.equalTo("user", userName);
        query.find({
            success: function (results) {
                for (seriesAndMoviesCount = 0; seriesAndMoviesCount < results.length; seriesAndMoviesCount++) {
                    seriesAndMovies.push(results[seriesAndMoviesCount].get("seriesItem"));
                }
                userNumberOfItemsAndItems = {
                    user: userName,
                    userName: userName.attributes.username,
                    numberOfSeriesAndMovies: seriesAndMovies.length,
                    seriesAndMovies: seriesAndMovies
                };
                allListsOfAllUsers = allListsOfAllUsers.concat(userNumberOfItemsAndItems);
                
                if (currentUserCount === totalNumberOfUser) {
                    that.notifyAll("dataOfUsersIsAvailable", allListsOfAllUsers);
                }
            }
        });
    }
    
    
    //Gets a List of all users which are registered in parse.com
    function createlistOfOtherUsers() {
        var userCount,
            user = Parse.Object.extend("_User"),
            query = new Parse.Query(user),
            allUser = [];
        
        query.find({
            success: function (result) {
                for (userCount = 0; userCount < result.length; userCount++) {
                    if (result[userCount].id !== Parse.User.current().id) {
                        allUser.push(result[userCount]);
                    }
                }
                that.notifyAll("listOfUsers", allUser);
            }
        });
    }

 
    //Gets a static list of series
    function getTop20Series() {
        var series = Parse.Object.extend("Top20Series"),
            query = new Parse.Query(series),
            count;
        top20Series = [];
        query.find().then(
            function (results) {
                for (count = 0; count < results.length; count++) {
                    top20Series.push(results[count].get("seriesItem"));
                }
                that.notifyAll("listFilled", top20Series);
            },
            function (error) {
                alert("Error: " + error.code + " " + error.message);
            }
        );
    }

    
    //Checks the current status of a user
    function getUserStatus() {
        checkLoginStatus();
    }

    
    //Returns the static list of top 20 series
    function getTop20() {
        getTop20Series();
    }
    
    
    //Initializes the connection to parse.com, checks if user is already loggedin and gets the top 20 series from parse.com
    function init() {
        Parse.initialize("Vq7NDAvGX8sl9rrcQyPcXSAnn6cyKIUqmgLa3s0V", "VvSJQZnp5SlHBEewRWPaYU2qVl5nORdNnrCCQWJl");
        storage = FavorizeController.SessionStorage;
    }
    
    
    that.init = init;
    that.getUserStatus = getUserStatus;
    that.getTop20 = getTop20;
    that.createlistOfOtherUsers = createlistOfOtherUsers;
    that.getSeriesOrMoviesOfSpecificUser = getSeriesOrMoviesOfSpecificUser;
    that.showListOfClickedUserInView = showListOfClickedUserInView;
    that.removeSeriesFromUserList = removeSeriesFromUserList;
    that.registerNewUser = registerNewUser;
    that.loginAsUser = loginAsUser;
    that.logoutAsUser = logoutAsUser;
    that.saveSeriesForUser = saveSeriesForUser;
    that.createUsersListOfSeries = createUsersListOfSeries;
    return that;
    
};