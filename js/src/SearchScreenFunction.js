var FavorizeController = FavorizeController || {};

FavorizeController.SearchScreenFunction = (function () {
    "use strict";
    /* eslint-env browser  */
    
    var that = {},
        storage,
        server,
        login,
        headerFunctions,
        view,
        inputOfUser,
        defaultNumberOfSite = 1,
        numberOfCurrentSite,
        inputSearchHeader;

    
    
    //Gets the search input of the user
    function getInputOfUser() {
        storage.loadUserInput();
        inputOfUser = storage.getUserInput();
        view.setUserInputHeader(inputOfUser);
        storage.setUserInput("", "save");
    }


    //Shows the found series/ movies on the screen
    function showDetailsOfSeriesOrMovie(event) {
        view.showSeriesOrFilmsInSearchScreenOrListView(event.data);
    }
    
    
    //Searches for more series or movies
    function loadMoreResults() {
        numberOfCurrentSite++;
        server.searchSeriesOrFilm(inputSearchHeader, numberOfCurrentSite);
    }
    
    
    //Shows series/ movies on the screen if more were found
    function appendMoreSearchResultsInView(event) {
        if (event.data.listOfSeries.length === 0) {
            loadMoreResults();
        } else {
            view.showMoreResultsInSearchScreen(event.data);
        }
    }


    //Shows buttons in the header depending on the status of the user
    function showDataDependingOnLoginStatus(event) {
        view.changeViewForLoginAndRegister(event.data);
    }


    //Shows the list of a user on the screen
    function showPersonalListOfCurrentUser() {
        location.assign("liste.html");
    }

    
    //Executes login and changes the buttons in the header
    function executeLogin(event) {
        login.loginAsUser(event.data);
        view.showOrHideLoginDialog();
    }

    
    //Executes registration and changes the buttons in the header
    function executeSignUp(event) {
        login.registerNewUser(event.data);
        view.showOrHideRegisterDialog();
    }

    
    //Logs out the current user
    function logoutTheCurrentUser() {
        login.logoutAsUser();
    }


    //Loads the list of the logged in user
    function getListOfCurrentUser() {
        login.createUsersListOfSeries("list");
    }


    //Searches for series/ movies with the input of the user
    function callSeriesInformation(event) {
        inputSearchHeader = event.data;
        numberOfCurrentSite = defaultNumberOfSite;
        server.searchSeriesOrFilm(inputSearchHeader, defaultNumberOfSite);
    }


    //Saves the clicked Series in the session storage and shows the details of the clicked series
    function showClickedSeriesInSearchScreen(event) {
        storage.setSeriesOrMovieItem(event.data, "save");
        location.assign("itemDetailView.html");
    }


    //Initializes the Event Listener
    function initializeEventListener() {
        server.addEventListener("firstPageOfResults", showDetailsOfSeriesOrMovie);
        server.addEventListener("showMoreResultsInView", appendMoreSearchResultsInView);
        login.addEventListener("loginStatusOfUser", showDataDependingOnLoginStatus);
        login.addEventListener("seriesOfAUserLoaded", showPersonalListOfCurrentUser);

        headerFunctions.addEventListener("loginButtonInDialogClicked", executeLogin);
        headerFunctions.addEventListener("registerButtonInDialogClicked", executeSignUp);
        headerFunctions.addEventListener("logoutCurrentUser", logoutTheCurrentUser);
        headerFunctions.addEventListener("listClicked", getListOfCurrentUser);

        headerFunctions.addEventListener("inputOfUserInHeaderSearchField", callSeriesInformation);

        view.addEventListener("userClickedASeriesInSearchScreenOrList", showClickedSeriesInSearchScreen);
        view.addEventListener("showMoreResultsClicked", loadMoreResults);
    }


    
    function init() {
        storage = FavorizeController.SessionStorage;
        login = FavorizeController.LoginScreen();
        server = FavorizeController.ServerConnector();
        view = FavorizeController.SeriesView();
        headerFunctions = FavorizeController.FunctionsInHeader();
        initializeEventListener();

        login.init();
        login.getUserStatus();

        getInputOfUser();
    }

    
    that.init = init;
    return that;
}());