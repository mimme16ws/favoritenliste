var FavorizeController = FavorizeController || {};

FavorizeController.SeriesView = function () {
    "use strict";
    /* eslint-env browser  */
    /* global EventPublisher */
    var that = new EventPublisher(),

        registerDialog = document.getElementById("register"),
        loginDialog = document.getElementById("login"),

        loginButtonTop = document.getElementById("login-Button"),
        logoutButtonTop = document.getElementById("logout-Button"),
        registerButtonTop = document.getElementById("register-Button"),
        listButtonTop = document.getElementById("list-Button"),

        searchField = document.getElementById("searchbox"),
        searchInHeader = document.getElementById("searchInHeader"),
        dataInCalender = document.getElementById("listname"),

        searchInputOfUser = document.getElementById("searchField"),
        searchFieldHeader = document.getElementById("search-header"),

        seriesList = document.querySelector("#series-items"),
        showMoreResultsSpace = document.querySelector("#show-more-results"),
        moviesList = document.querySelector("#movie-items"),

        spaceForFirstResults = document.getElementById("series-items"),

        seriesListOfAUser,
        listOfUsersInListView,
        imageNotAvailableSource = "res/images/imageNotAvailable.png",

        noUser = "false",
        

        currentSiteShown = location.pathname.split("/").slice(-1)[0];



    //Depending on the current screen the buttons in the header are shown or hidden
    function changeViewForLoginAndRegister(userStatus) {
        if (currentSiteShown !== "index.html") {
            searchInHeader.style.display = "block";
        } else {
            searchInHeader.style.display = "none";
        }
        if (userStatus !== noUser) {
            if (currentSiteShown === "liste.html") {
                logoutButtonTop.style.display = "block";
            } else {
                listButtonTop.style.display = "block";
                logoutButtonTop.style.display = "block";
                registerButtonTop.style.display = "none";
                loginButtonTop.style.display = "none";
                
                if (currentSiteShown === "index.html") {
                    dataInCalender.innerHTML = "Series this month";
                }
            }
        } else {
            listButtonTop.style.display = "none";
            logoutButtonTop.style.display = "none";
            registerButtonTop.style.display = "block";
            loginButtonTop.style.display = "block";
            
            if (currentSiteShown === "index.html") {
                dataInCalender.innerHTML = "Top 20 Series";
            }
        }
    }


    //Shows or hides login window and the overlay
    function showOrHideLoginDialog() {
        loginDialog.classList.toggle("md-show");
        if (loginDialog.classList.contains("md-show")) {
            setTimeout(function () {
                $("#user-login").focus();
            }, 500);
        }
    }


    //Shows or hides register window and the overlay
    function showOrHideRegisterDialog() {
        registerDialog.classList.toggle("md-show");
        if (registerDialog.classList.contains("md-show")) {
            setTimeout(function () {
                $("#user-register").focus();
            }, 500);
        }
    }

    //Shows or hides login and register window and the overlay
    function hideRegisterOrLoginDialog() {
        if (loginDialog.classList.contains("md-show")) {
            showOrHideLoginDialog();
        }
        if (registerDialog.classList.contains("md-show")) {
            showOrHideRegisterDialog();
        }
    }
    
    
    //Shows the list of series/ movies of the clicked user
    function showListOfClickedUser(event) {
        var numberOfClasses = event.target.classList.length - 1,
            clickedUser = event.target.classList[numberOfClasses];
        
        that.notifyAll("clickedUserInListViewScreen", listOfUsersInListView[clickedUser].user);
    }
    
    
    //Shows other users on the list screen. Also shows how many series/ movies they have in their list and how many are in common
    function showOtherUsersSeriesAndMoviesInList(listOfUsers) {
        var numberOfUsersToShow = 5,
            userList = document.querySelector("#list-other-user"),
            userCount,
            userName,
            usersNumberOfSeries,
            usersNumberOfMovies,
            usersTotalNumberOfItems,
            oneUserRow;
        
        listOfUsersInListView = listOfUsers;
        
        if (listOfUsers.length < numberOfUsersToShow) {
            numberOfUsersToShow = listOfUsers.length;
        }
        
        for (userCount = 0; userCount < numberOfUsersToShow; userCount++) {
            userName = listOfUsers[userCount].userName;
            usersNumberOfSeries = listOfUsers[userCount].commonSeries;
            usersNumberOfMovies = listOfUsers[userCount].commonMovies;
            usersTotalNumberOfItems = listOfUsers[userCount].totalNumberOfSeriesAndMovies;
            oneUserRow = Node.fromString("<div class='user-in-list " + userCount + "' id='user-" + userCount + "'>" +
                    "<div class='" + userCount + "'>User: <b class='" + userCount + "'>" + userName + "</b><br></div>" +
                    "<div class='" + userCount + "'>" +
                        "<p class='user-information " + userCount + "'>" +
                            "Total: <b class='" + userCount + "'>" + usersTotalNumberOfItems + "</b> items in list<br>" +
                            "<b class='" + userCount + "'>" + usersNumberOfSeries + "</b> series and <b class='" + userCount + "'>" + usersNumberOfMovies + "</b> movies in common" +
                        "</p>" +
                "</div>");
            oneUserRow.addEventListener("click", showListOfClickedUser);
            userList.appendChild(oneUserRow);
        }
    }
    
    
    //Scrolls to the first new found series/ movie
    function scrollToFirstFoundItem(firstNewFoundItem) {
        $("html, body").animate({
            scrollTop: $("#series" + firstNewFoundItem).offset().top
        }, 1000);
    }
    

    //Gets the clicked series or movie from the saved array
    function showSeries(event) {
        var numberOfClasses = event.target.classList.length - 1,
            clickedSeriesID = event.target.classList[numberOfClasses];

        if (seriesListOfAUser[clickedSeriesID] !== undefined) {
            that.notifyAll("userClickedASeriesInSearchScreenOrList", seriesListOfAUser[clickedSeriesID]);
        }
    }

    
    //Shows more results on the screen and scrolls to the first new found series or film
    function showMoreResultsInSearchScreen(moreResults) {
        var numbOfSeries,
            nameOfSeries,
            imdbRating,
            actors,
            type,
            imageSource,
            oneSeriesRow;
        
        seriesListOfAUser = seriesListOfAUser.concat(moreResults.listOfSeries);
        
        for (numbOfSeries = seriesListOfAUser.length - moreResults.listOfSeries.length; numbOfSeries < seriesListOfAUser.length; numbOfSeries++) {
            nameOfSeries = seriesListOfAUser[numbOfSeries].Title;
            imdbRating = seriesListOfAUser[numbOfSeries].imdbRating;
            actors = seriesListOfAUser[numbOfSeries].Actors;
            type = seriesListOfAUser[numbOfSeries].Type;

            if (seriesListOfAUser[numbOfSeries].Poster !== "N/A") {
                imageSource = seriesListOfAUser[numbOfSeries].Poster;
            } else {
                imageSource = imageNotAvailableSource;
            }

            oneSeriesRow = Node.fromString("<div class='row series " + numbOfSeries + "' id='series" + numbOfSeries + "'>" +
                "<div class='c2'>" +
                    "<img class='image " + numbOfSeries + "' src='" + imageSource + "'>" +
                "</div>" +
                "<div class='c10'>" +
                    "<div class='title " + numbOfSeries + "'>" + nameOfSeries + "</div>" +
                    "<div class='" + numbOfSeries + "'>Rating: " + imdbRating + "</div>" +
                    "<div class='" + numbOfSeries + "'>Actors: " + actors + "</div>" +
                    "<div class='" + numbOfSeries + "'>This is a <b>" + type + "</b></div>" +
                "</div>");
            oneSeriesRow.addEventListener("click", showSeries);
            seriesList.appendChild(oneSeriesRow);
        }
        scrollToFirstFoundItem(seriesListOfAUser.length - moreResults.listOfSeries.length);

        if (moreResults.numberOfPages === moreResults.totalNumberOfPages) {
            $("#show-more").hide(500);
        }
    }
    
    
    //Shows series when the top series button is clicked
    function showSeriesInListScreen() {
        $("#series-items").show(500);
        $("#movie-items").hide(500);
        document.getElementById("series-show").disabled = true;
        document.getElementById("movies-show").disabled = false;
    }
    
    
    //Shows movies when the top movie button is clicked
    function showMoviesInListScreen() {
        $("#series-items").hide(500);
        $("#movie-items").show(500);
        document.getElementById("series-show").disabled = false;
        document.getElementById("movies-show").disabled = true;
    }
    
    
    //Check if the current screen the personal list of a user is or the search screen
    function checkIfSearchOrListScreen(listOrResults) {
        if (currentSiteShown === "search.html") {
            seriesListOfAUser = listOrResults.listOfSeries;

            if (spaceForFirstResults.innerHTML !== "") {
                $("#series-items").hide();
                $("#show-more-results").hide();
                document.getElementById("series-items").innerHTML = "";
                document.getElementById("show-more-results").innerHTML = "";
            }
        } else {
            seriesListOfAUser = listOrResults;
        }
    }


    //Hides a removed series or movie in the list of a user
    function hideDeletedSeriesInList(seriesOrMovieToRemove) {
        var objectToRemove = "#series" + seriesOrMovieToRemove;
        $(objectToRemove).hide(500);
    }


    //Deletes a series/ movie from the parse.com database and hides the displayed object
    function removeSeries(event) {
        var itemNumberInSeriesList = event.target.id,
            itemToDelete = seriesListOfAUser[itemNumberInSeriesList];
        
        that.notifyAll("removeSeriesOrMovieFromPersonalList", itemToDelete);

        hideDeletedSeriesInList(itemNumberInSeriesList);
    }
    

    //The button show more results was clicked
    function showMoreSearchResults() {
        that.notifyAll("showMoreResultsClicked", null);
    }

    
    //Returns to the top of the screen
    function goToTopOfSite() {
        $("html, body").animate({
            scrollTop: $(".site-container").offset().top
        }, 1000);
    }


    //Shows either found series/ movies on the search screen, list screen or shows a message that no series/ films were found/ available
    function showSeriesOrFilmsInSearchScreenOrListView(searchResultsOrPersonalList) {
        var oneSeriesRow,
            seriesItemCount,
            imageSource,
            nameOfSeries,
            imdbRating,
            actors,
            type,
            showResults,
            backtoTopButton,
            seriesCount = 0,
            movieCount = 0;
        
        checkIfSearchOrListScreen(searchResultsOrPersonalList);
        
        if (seriesListOfAUser !== undefined) {
            for (seriesItemCount = 0; seriesItemCount < seriesListOfAUser.length; seriesItemCount++) {
                nameOfSeries = seriesListOfAUser[seriesItemCount].Title;
                imdbRating = seriesListOfAUser[seriesItemCount].imdbRating;
                actors = seriesListOfAUser[seriesItemCount].Actors;
                type = seriesListOfAUser[seriesItemCount].Type;

                if (seriesListOfAUser[seriesItemCount].Poster !== "N/A") {
                    imageSource = seriesListOfAUser[seriesItemCount].Poster;
                } else {
                    imageSource = imageNotAvailableSource;
                }

                oneSeriesRow = Node.fromString("<div class='row series " + seriesItemCount + "' id='series" + seriesItemCount + "'>" +
                    "<div class='c2'>" +
                        "<img class='image " + seriesItemCount + "' src='" + imageSource + "'>" +
                    "</div>" +
                    "<div class='c10 " + seriesItemCount + "'>" +
                        "<div class='title " + seriesItemCount + "'>" + nameOfSeries + "</div>" +
                        "<div class='" + seriesItemCount + "'>Rating: " + imdbRating + "</div>" +
                        "<div class='" + seriesItemCount + "'>Actors: " + actors + "</div>" +
                        "<div id='type" + seriesItemCount + "' class='" + seriesItemCount + "'>This is a <b>" + type + "</b></div>" +
                        "<button id='" + seriesItemCount + "' class='deleteButton end' type='button'>Delete</button>" +
                    "</div>");

                oneSeriesRow.addEventListener("click", showSeries);

                if (currentSiteShown === "search.html") {
                    seriesList.appendChild(oneSeriesRow);
                    document.getElementById(seriesItemCount).style.display = "none";
                } else {
                    if (type === "series") {
                        seriesCount++;
                        seriesList.appendChild(oneSeriesRow);
                    } else {
                        movieCount++;
                        moviesList.appendChild(oneSeriesRow);
                    }
                    if (currentSiteShown === "liste.html") {
                        document.getElementById(seriesItemCount).addEventListener("click", removeSeries);
                        document.getElementById("type" + seriesItemCount).style.display = "none";
                    } else {
                        document.getElementById(seriesItemCount).style.display = "none";
                    }
                }
            }

            if (currentSiteShown === "search.html") {
                showResults = Node.fromString("<button type='button' id='show-more'>Show more results</button>");
                backtoTopButton = Node.fromString("<button type='button' id='back-to-top'>Back to Top</button>");

                showResults.addEventListener("click", showMoreSearchResults);
                backtoTopButton.addEventListener("click", goToTopOfSite);
                showMoreResultsSpace.appendChild(showResults);
                showMoreResultsSpace.appendChild(backtoTopButton);
            } else if (currentSiteShown === "liste.html") {
                if (seriesCount === 0) {
                    document.getElementById("no-series").style.display = "block";
                }
                if (movieCount === 0) {
                    document.getElementById("no-movies").style.display = "block";
                }
            }
            
        } else {
            
            oneSeriesRow = Node.fromString("<div class='row series'>" +
                    "<div class='c2'>" +
                        "<img src='" + imageNotAvailableSource + "'>" +
                    "</div>" +
                    "<div class='c8'>" +
                        "<div class='title'><b>Series/ Film does not exist!</div>" +
                    "</div>");
            seriesList.appendChild(oneSeriesRow);
        }
        
        $("#series-items").show(500);
        if (searchResultsOrPersonalList.totalNumberOfPages !== 1) {
            $("#show-more-results").show(500);
        }
    }


    //Shows the data in the detail view of the series or movie
    function showSeriesData(seriesItem) {
        if (seriesItem.Poster === "N/A") {
            document.getElementById("itemImageDetail").src = imageNotAvailableSource;
        } else {
            document.getElementById("itemImageDetail").src = seriesItem.Poster;
        }
        document.getElementById("itemNameDetail").innerHTML = seriesItem.Title;
        document.getElementById("itemGenreDetail").innerHTML = "<b>Genre:</b><br>" + seriesItem.Genre;
        document.getElementById("itemReleaseDetail").innerHTML = "<b>Release:</b><br>" + seriesItem.Released;
        document.getElementById("itemRaringDetail").innerHTML = "<b>IMDB Rating: </b>" + seriesItem.imdbRating;
        document.getElementById("itemRuntimeDetail").innerHTML = "<b>Runtime:</b><br>" + seriesItem.Runtime;
        document.getElementById("itemYearDetail").innerHTML = "<b>Year:</b><br>" + seriesItem.Year;
        document.getElementById("itemActorsDetail").innerHTML = "<b>Cast:</b><br>" + seriesItem.Actors;
        document.getElementById("itemPlotDetail").innerHTML = seriesItem.Plot;
        document.getElementById("itemCreatorDetail").innerHTML = "<b>Creator:</b><br>" + seriesItem.Writer;
        document.getElementById("itemAwardsDetail").innerHTML = "<b>Awards:</b><br>" + seriesItem.Awards;
        document.getElementById("itemLanguageDetail").innerHTML = "<b>Language:</b><br>" + seriesItem.Language;
        document.getElementById("itemIMDBVotesDetail").innerHTML = "<b>IMDB - Votes:</b><br>" + seriesItem.imdbVotes;
    }


    //Initializes the search field on the home/ start screen
    function initializeSearchField() {
        searchField.onkeyup = function () {
            var inputOfTheUser = searchInputOfUser.value;
            that.notifyAll("userSearchInput", inputOfTheUser);
            searchInputOfUser.value = "";
        };
    }


    //Sets the input of a user which was entered in the home/ start screen
    function setUserInputHeader(searchTerm) {
        searchFieldHeader.value = searchTerm;
        searchFieldHeader.focus();
    }


    that.initializeSearchField = initializeSearchField;
    that.showSeriesOrFilmsInSearchScreenOrListView = showSeriesOrFilmsInSearchScreenOrListView;
    that.showSeriesInListScreen = showSeriesInListScreen;
    that.showMoviesInListScreen = showMoviesInListScreen;
    that.showMoreResultsInSearchScreen = showMoreResultsInSearchScreen;
    that.showOtherUsersSeriesAndMoviesInList = showOtherUsersSeriesAndMoviesInList;
    that.setUserInputHeader = setUserInputHeader;
    that.changeViewForLoginAndRegister = changeViewForLoginAndRegister;
    that.showOrHideLoginDialog = showOrHideLoginDialog;
    that.showOrHideRegisterDialog = showOrHideRegisterDialog;
    that.hideRegisterOrLoginDialog = hideRegisterOrLoginDialog;
    that.showSeriesData = showSeriesData;
    return that;
};
