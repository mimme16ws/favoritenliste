var FavorizeController = FavorizeController || {};

FavorizeController.ServerConnector = function () {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher, Ajax */
    
    var that = new EventPublisher(),
        falseResp = "False",
        totalSeries,
        seriesFind = [],
        allSeasonsForDetails = [],
        numberOfResulsPages,
        numberOfCurrentPage,
        numberOfFoundSeries,

        count = 0,
        countOfResults;
    
    
    
    //Sends the found series or movies to the view if the necessary conditions are met
    function callbackOfCurrentResult(data) {
        var seriesAndNumberOfTotalPages,
            dataToDisplayInSearchScreen;
        
        count++;

        if (data.Type === "series" || data.Type === "movie") {
            seriesFind.push(data);
        }

        if (count === countOfResults) {
            if (seriesFind.length === numberOfFoundSeries) {
                if (numberOfCurrentPage === 1) {
                    seriesAndNumberOfTotalPages = {
                        totalNumberOfPages: numberOfResulsPages,
                        listOfSeries: seriesFind
                    };
                    that.notifyAll("firstPageOfResults", seriesAndNumberOfTotalPages);
                } else {
                    dataToDisplayInSearchScreen = {
                        numberOfPages: numberOfCurrentPage,
                        totalNumberOfPages: numberOfResulsPages,
                        listOfSeries: seriesFind
                    };
                    that.notifyAll("showMoreResultsInView", dataToDisplayInSearchScreen);
                }
            }
        }
    }
    
    
    //Gets informations of a series/ movie with the specific ID
    function searchforCurrentResult(currentResultID) {
        var url = "http://www.omdbapi.com/?i=" + currentResultID + "&y=&plot=full&r=json&season";
        Ajax.call({
            method: "GET",
            dataType: "json",
            url: url,
            success: callbackOfCurrentResult
        });
    }
    
    
    //Counts the found results, number of pages and the number of relevant results of the specific page of results
    function callbackSearchResults(results) {
        var searchResults,
            resultCount,
            currentResult;
            
        if (results.Response === falseResp) {
            that.notifyAll("firstPageOfResults", falseResp);
        } else {
            numberOfResulsPages = Math.ceil(results.totalResults / 10);
            seriesFind = [];
            count = 0;
            countOfResults = results.Search.length;
            numberOfFoundSeries = 0;


            for (searchResults = 0; searchResults < results.Search.length; searchResults++) {
                if (results.Search[searchResults].Type === "series" || results.Search[searchResults].Type === "movie") {
                    numberOfFoundSeries++;
                }
            }
            for (resultCount = 0; resultCount < results.Search.length; resultCount++) {
                currentResult = results.Search[resultCount];
                searchforCurrentResult(currentResult.imdbID);
            }
        }
    }
    
    
    //Searches for series/ movies on the specific page
    function searchSeriesOrFilm(usersInput, pageNumb) {
        var url;
        numberOfCurrentPage = pageNumb;
        url = "http://www.omdbapi.com/?s=" + usersInput + "&page=" + numberOfCurrentPage;
        Ajax.call({
            method: "GET",
            dataType: "json",
            url: url,
            success: callbackSearchResults
        });
    }
    
    
    //Searches for a specific season
    function callForSeasonsCount(imdbId, count, list) {
        var url = "http://www.omdbapi.com/?i=" + imdbId + "&Season=" + count;
        Ajax.call({
            method: "GET",
            dataType: "json",
            url: url,
            success: callbackSeasonsCount.bind(this, count, imdbId, list)
        });
    }
    
    
    //Checks for the latest season
    function callbackSeasonsCount(count, imdbId, list, data) {
        if (data.Response === "False") {
            callbackEpisodes(list);
        } else {
            list.push(data);
            callForSeasonsCount(imdbId, count + 1, list);
        }
    }
    
    
    //Initializes the Ajax call for the season informations
    function getSeasonInformation(series) {
        var currentSeries,
            imdbId,
            startCount,
            allSeasonsForCalendar;
        totalSeries = series.length;
        for (currentSeries = 0; currentSeries < totalSeries; currentSeries++) {
            if (series[currentSeries].Type === "movie") {
                continue;
            }
            imdbId = series[currentSeries].imdbID;
            startCount = 1;
            allSeasonsForCalendar = [];
            callForSeasonsCount(imdbId, startCount, allSeasonsForCalendar);
        }
    }

    
    //Initializes the Ajax call for the season informations of the series
    function loadSeries(series) {
        getSeasonInformation(series);
    }
    
    //notifies the calendar
    function callbackEpisodes(list) {
        that.notifyAll("showEpisodesDetails", list);
    }
    
    
    //Initializes the Ajax call for all season informations of one spesific series
    function searchSeasonDetails(series) {
        var id,
            startCount;
        id = series.imdbID;
        startCount = 1;
        if (series.Type === "series") {
            callForAllSeasons(id, startCount);
        }
    }
    
    
    //Ajax call for all season informations of one spesific series
    function callForAllSeasons(id, counter) {
        var url = "http://www.omdbapi.com/?i=" + id + "&Season=" + counter;
        Ajax.call({
            method: "GET",
            dataType: "json",
            url: url,
            success: callbackForAllSeasons.bind(this, counter, id)
        });
    }
    
    
    //Checks for the latest season
    function callbackForAllSeasons(counter, id, data) {
        if (data.Response === "False") {
            SeasonDetails();
        } else {
            allSeasonsForDetails.push(data);
            callForAllSeasons(id, counter + 1);
        }
    }
    
    
    //notifies the seasonDetails
    function SeasonDetails() {
        that.notifyAll("SeasonDetailsLoaded", allSeasonsForDetails);
    }
    
    
    that.searchSeasonDetails = searchSeasonDetails;
    that.loadSeries = loadSeries;
    that.searchSeriesOrFilm = searchSeriesOrFilm;
    return that;
};