var FavorizeController = FavorizeController || {};

FavorizeController.SessionStorage = (function () {
    "use strict";
    
    var that = {},
        LIST_KEY = "LIST",
        SERIES_ITEM_KEY = "SERIES",
        SEARCH_INPUT_KEY = "SEARCH",
        LIST_OF_USER_KEY = "USERLIST",
        list,
        seriesItem,
        searchInput,
        userList;
    
    
    
    //Loads/ parse the saved series/ movie list from the session storage
    function loadSeriesList() {
        list = JSON.parse(sessionStorage.getItem(LIST_KEY));
    }

    
    //Saves the list of series/ movies in the session storage
    function saveSeriesList() {
        sessionStorage.setItem(LIST_KEY, JSON.stringify(list));
    }

    
    //Checks if list of series/ movies should be saved in the session storage
    function setList(newList, saveNow) {
        list = newList;
        if (saveNow) {
            saveSeriesList();
        }
    }

    
    //Returns the saved list in the session storage
    function getList() {
        return list;
    }

    
    //Delets the saved list of series/ movies in the session storage
    function clearList() {
        sessionStorage.removeItem(LIST_KEY);
    }


    //Loads/ parse the saved series/ movie from the session storage
    function loadSeriesOrMovieItem() {
        seriesItem = JSON.parse(sessionStorage.getItem(SERIES_ITEM_KEY));
    }

    
    //Saves a series/ movies in the session storage
    function saveSeriesItem() {
        sessionStorage.setItem(SERIES_ITEM_KEY, JSON.stringify(seriesItem));
    }

    
    //Checks if series/ movies should be saved in the session storage
    function setSeriesOrMovieItem(newSeriesItem, saveNow) {
        seriesItem = newSeriesItem;
        if (saveNow) {
            saveSeriesItem();
        }
    }

    
    //Returns the saved series/ movie in the session storage
    function getSeriesOrMovieItem() {
        return seriesItem;
    }


    //Loads/ parse the saved user input from the session storage
    function loadUserInput() {
        searchInput = JSON.parse(sessionStorage.getItem(SEARCH_INPUT_KEY));
    }

    
    //Saves the users input in the session storage
    function saveUserInput() {
        sessionStorage.setItem(SEARCH_INPUT_KEY, JSON.stringify(searchInput));
    }

    
    //Checks if the input of the user should be saved in the session storage
    function setUserInput(inputOfUser, saveNow) {
        searchInput = inputOfUser;
        if (saveNow) {
            saveUserInput();
        }
    }

    
    //Returns the saved input of a user in the session storage
    function getUserInput() {
        return searchInput;
    }


    //Loads/ parse the saved list of the clicked user from the session storage
    function loadListOfClickedUser() {
        userList = JSON.parse(sessionStorage.getItem(LIST_OF_USER_KEY));
    }

    
    //Saves the users list in the session storage
    function saveUsersList() {
        sessionStorage.setItem(LIST_OF_USER_KEY, JSON.stringify(userList));
    }

    
    //Checks if the list of the clicked user should be saved in the session storage
    function setListOfClickedUser(inputOfUser, saveNow) {
        userList = inputOfUser;
        if (saveNow) {
            saveUsersList();
        }
    }

    
    //Returns the saved list of clicked user in the session storage
    function getListOfClickedUser() {
        return userList;
    }
    
    

    that.loadSeriesList = loadSeriesList;
    that.setList = setList;
    that.getList = getList;
    that.clearList = clearList;

    that.loadSeriesOrMovieItem = loadSeriesOrMovieItem;
    that.setSeriesOrMovieItem = setSeriesOrMovieItem;
    that.getSeriesOrMovieItem = getSeriesOrMovieItem;

    that.loadUserInput = loadUserInput;
    that.setUserInput = setUserInput;
    that.getUserInput = getUserInput;
    
    that.loadListOfClickedUser = loadListOfClickedUser;
    that.setListOfClickedUser = setListOfClickedUser;
    that.getListOfClickedUser = getListOfClickedUser;
    return that;
}());